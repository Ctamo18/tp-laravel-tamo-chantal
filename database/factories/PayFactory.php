<?php

namespace Database\Factories;

use App\Models\Pay;
use Illuminate\Database\Eloquent\Factories\Factory;

class PayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pay::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle'=>$this->faker->country,
            'description'=>$this->faker->sentence,
            'code_indicatif'=>$this->faker->countryCode,
            'continent'=>$this->faker->randomElement(["Afrique","Asie","Europe","Amerique"]),
            'population'=>$this->faker->randomElement([7000,5000,800,400]),
            'capitale'=>$this->faker->city,
            'monnaie'=>$this->faker->randomElement(["XOF","EURO","DOLLAR"]),
            'langue'=>$this->faker->randomElement(["FR","EN","AR","ES"]),
            'superficie'=>$this->faker->randomElement([20,200,3642,595]),
            'est_laique'=>$this->faker->boolean
        ];
    }
}
