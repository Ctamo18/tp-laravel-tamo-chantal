<?php

namespace App\Http\Controllers;

use App\Models\Pay;
use Illuminate\Http\Request;

class PayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('includes.pays.index', [
            "pays"=>Pay::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('includes.pays.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "libelle"=> 'required',
            "capitale"=> 'required',
            "monnaie"=> 'required'

         ]);

         //Enregistrement dans la bd
         $produit = Pay::create([
             "libelle" =>$request->get('libelle'),
             "description"=> $request->get('description'),
             "code_indicatif"=>$request->get('code_indicatif'),
             "continent" =>$request->get('continent'),
             "population" =>$request->get('population'),
             "capitale" =>$request->get('capitale'),
             "monnaie" =>$request->get('monnaie'),
             "langue" =>$request->get('langue'),
             "superficie" =>$request->get('superficie'),
             "est_laique" =>$request->get('est_laique')
         ]);

         return redirect()->route("pays.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function show(Pay $pay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function edit(Pay $pay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pay $pay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pay  $pay
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pay $pay)
    {
        //
    }
}
