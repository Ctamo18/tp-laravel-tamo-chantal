@extends('layouts.main')

@section('content')
<div class="col-md-8">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Edit Table</h4>
        <p class="card-category">Complete your table</p>
      </div>
      <form class="form-horizontal" action="{{route('pays.store')}}" method="POST" >
        @method("POST")
        @csrf
      <div class="card-body">
        <form>
          <div class="row">
            <div class="col-md-3">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Libelle</label>
                <input type="text" name="libelle" class="form-control" >
              </div>
            </div>
            <div class="col-md-3">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Code Indicatif</label>
                  <input type="text" name="code_indicatif" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Continent</label>
                  <input type="text" name="continent" class="form-control">
                </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-12">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Description</label>
                  <input type="text" name="description" class="form-control">
                </div>
              </div>

          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Population</label>
                <input type="number" name="population" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Capitale</label>
                  <input type="text" name="capitale" class="form-control">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                 <!-- <label class="bmd-label-floating">Langue</label>
                  <input type="text" name="langue" class="form-control"> -->
                  <select name="langue" class="form-control">
                      <option value="">Langue</option>
                      <option value="FR">FR</option>
                      <option value="EN">EN</option>
                      <option value="AR">AR</option>
                      <option value="ES">ES</option>
                  </select>
                </div>
              </div>
          </div>


          <div class="row">
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
               <!-- <label class="bmd-label-floating">Monnaie</label>
                <input type="text" name="monnaie" class="form-control">-->
                <select name="monnaie" title="Monnaie" class="form-control">
                    <option value="">Monnaie</option>
                    <option value="XOF">XOF</option>
                    <option value="EUR">EUR</option>
                    <option value="DOLLAR">DOLLAR</option>
                </select>
              </div>
            </div>

            <div class="row">
            <div class="col-md-6">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Superficie</label>
                  <input type="text" name="superficie" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Est Laique</label>
                  <input type="text" name="est_laique" class="form-control">
                </div>
              </div>

          </div>
          <button type="submit" class="btn btn-primary pull-right">Update Table</button>


          <div class="clearfix"></div>
        </form>
      </div>
      </form>
    </div>
  </div>

@endsection
