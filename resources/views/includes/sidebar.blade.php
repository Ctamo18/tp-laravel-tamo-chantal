<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{asset('../assets/img/sidebar-1.jpg')}}">
    <div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-normal">
        Creative Chantal
      </a></div>
        <div class="sidebar-wrapper">
             <ul class="nav">
                <li class="nav-item ">
                    <a class="nav-link" href="{{url("./pays/form")}}">
                        <i class="material-icons">person</i>
                        <p>Form</p>
                    </a>
                 </li>
                <li class="nav-item ">
                     <a class="nav-link" href="{{url("./pays")}}">
                        <i class="material-icons">content_paste</i>
                        <p>Table List</p>
                    </a>
                 </li>
             </ul>
          </div>
  </div>

